import sbt._

object Dependencies {

  object Version {
    val AkkaHttp               = "10.1.10"
    val AkkaStream             = "2.5.23"
    val AkkaHttpCirce          = "1.29.1"
    val Postgres               = "42.2.6"
    val Logback                = "1.2.3"
    val PureConfig             = "0.12.1"
    val ScalaTest              = "3.0.8"
    val ScalaCheck             = "1.14.1"
    val Circe                  = "0.12.0"
    val Cats                   = "2.0.0"
    val Doobie                 = "0.8.2"
    val TestContainers         = "0.33.0"
    val TestContainersPostgres = "1.12.2"
    val Sttp                   = "1.6.4"
    val FLyway                 = "6.0.4"
    val Tapir                  = "0.11.4"
  }

  lazy val AkkaHttp               = "com.typesafe.akka"      %% "akka-http"                      % Version.AkkaHttp
  lazy val AkkaStream             = "com.typesafe.akka"      %% "akka-stream"                    % Version.AkkaStream
  lazy val AkkaHttpCirce          = "de.heikoseeberger"      %% "akka-http-circe"                % Version.AkkaHttpCirce
  lazy val Postgres               = "org.postgresql"         % "postgresql"                      % Version.Postgres
  lazy val PureConfig             = "com.github.pureconfig"  %% "pureconfig"                     % Version.PureConfig
  lazy val PureConfigCatsEffect   = "com.github.pureconfig"  %% "pureconfig-cats-effect"         % Version.PureConfig
  lazy val Logback                = "ch.qos.logback"         % "logback-classic"                 % Version.Logback
  lazy val ScalaTest              = "org.scalatest"          %% "scalatest"                      % Version.ScalaTest % "it,test"
  lazy val ScalaCheck             = "org.scalacheck"         %% "scalacheck"                     % Version.ScalaCheck % "it,test"
  lazy val CirceCore              = "io.circe"               %% "circe-core"                     % Version.Circe
  lazy val CirceParser            = "io.circe"               %% "circe-parser"                   % Version.Circe
  lazy val CirceGeneric           = "io.circe"               %% "circe-generic"                  % Version.Circe
  lazy val CatsCore               = "org.typelevel"          %% "cats-core"                      % Version.Cats
  lazy val CatsEffect             = "org.typelevel"          %% "cats-effect"                    % Version.Cats
  lazy val Doobie                 = "org.tpolecat"           %% "doobie-core"                    % Version.Doobie
  lazy val DoobiePostgres         = "org.tpolecat"           %% "doobie-postgres"                % Version.Doobie
  lazy val TestContainers         = "com.dimafeng"           %% "testcontainers-scala"           % Version.TestContainers % "it"
  lazy val TestContainersPostgres = "org.testcontainers"     % "postgresql"                      % Version.TestContainersPostgres % "it"
  lazy val Sttp                   = "com.softwaremill.sttp"  %% "core"                           % Version.Sttp % "it,test"
  lazy val SttpCats               = "com.softwaremill.sttp"  %% "async-http-client-backend-cats" % Version.Sttp % "it,test"
  lazy val SttpCirce              = "com.softwaremill.sttp"  %% "circe"                          % Version.Sttp % "it,test"
  lazy val FLyway                 = "org.flywaydb"           % "flyway-core"                     % Version.FLyway
  lazy val Tapir                  = "com.softwaremill.tapir" %% "tapir-core"                     % Version.Tapir
  lazy val TapirAkkaHttp          = "com.softwaremill.tapir" %% "tapir-akka-http-server"         % Version.Tapir
  lazy val TapirAkkaHttpSwagger   = "com.softwaremill.tapir" %% "tapir-swagger-ui-akka-http"     % Version.Tapir
  lazy val TapirDocs              = "com.softwaremill.tapir" %% "tapir-openapi-docs"             % Version.Tapir
  lazy val TapirOpenApiCirceYaml  = "com.softwaremill.tapir" %% "tapir-openapi-circe-yaml"       % Version.Tapir
  lazy val TapirCirceJson         = "com.softwaremill.tapir" %% "tapir-json-circe"               % Version.Tapir
}
