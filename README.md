# hiring-assignment-group-feed

Group Feed - Web service exposing REST API - Hiring Assignment for MeWe

## Requirements
You're required to write a backend + REST APIs for a simple posting in groups. The application will allow users to post into groups and read post feeds

    - let's assume all groups are "open", so any user can join any group as they wish
    - user must be a member of the group in order to post into it or read it's feeds
    - every group needs to have a feed, that is essentially a list of posts sorted from newest to oldest
    - there is also an "All groups feed" that displays feed of all posts for all groups user is a member of, sorted from newest to oldest
    - for simplicity, we'll assume that posts can't be edited or deleted once it's posted.
    - for the simplicity, users cannot leave group once becoming a member
    - for the simplicity, we'll assume text-only posts, no images, no attachments, no text formatting
    - for simplicity, there will be no comments
    - we will assume groups are already created, any valid integer is assumed to be a valid group ID of an existing group
    - post data in the feet must contain: ID, datetime created, content, author's user ID, author's name: TODO
    - post data in "All groups feed" must also contain group ID: TODO
    - keep in mind performance of "All groups feed" when the database will be really large

 Required APIs:
 
    - get list of groups user is member of (in our simplified version it'll be just the list of IDs)
    - become member of a group
    - add post to a group
    - get post feed of a group
    - get "All groups feed" (post feed of all groups user is member of)

Constraints:

    - let's assume User to be defined as Name and ID (no avatar or profile)
    - let's assume highly simplified authentication (eg. user ID serves as auth token) or even no authentication
    - choose whatever stack you want and whatever persistance you want, the only requirement is that you use Scala and Akka (in any way you see fit)
    - application should be easy to run on localhost via `sbt run` or similar. If there are some external requirements that need to be installed
      prior to running it (eg. some database, etc), please add a ReadMe file with a necessary description
    - deliver your solution on any git-based system where we can access it (GitHub, GitLab, etc.). Might even be private, as long as we get read permissions to it
    - preferably, task should be done as a normal project, with each step being identified as a commit instead of a single commit for the whole thing

Above are minimum requirements. You're welcome to come up with your own improvements if you wish so, but only as long as complexity is the same or higher than above.

## Description

Both groups and Users are created upfront.
By default there are 7 groups (ids: 1,2,3,4,5,6,7) and 4 users ((1, adam), (2, michal), (3, jan), (4, alex)).

It can be changed by modifying migration file.

Some endpoints requires authentication. Following header needs to be added when using this endpoints.
```
Authorization: Bearer userId
```
where userId is valid userID

User needs to subscribe to the group before posting posts there.
 
API documentation is available here: http://localhost:8080/docs (you need to run application first)

## Assumptions & Approach

- Akka Http as HTTP server
- Migrations done automatically (using wildfly)
- The goal I made for myself was to have fully documented REST API
- I wanted to be consistent with HETOAS approach    
- Initially I wanted to have both unit and integration (e2e) tests, but eventually I haven't written any unit test. 
This is because I was writting e2e tests as I was implementing the features and when I finished there was not point of adding unit tests.
- [Tapir](https://github.com/softwaremill/tapir) is (IMHO) awesome library - this is something I haven't been using before, 
but I am happy with a results and I plan to use it in future. 
- I haven't implemented pagination for all endpoints (because of the time limitation, and because they will look pretty same). 
The one which has pagination is the endpoint to list all feeds
- I've chosen the cursor pagination over the offset pagination due to performance requirement


## How to run

1. You need to setup DB (postgres in our case). 
The easiest way to do this is to just run the following command:
    ```bash
    docker-compose -f docker-compose.yml up -d
    ```
2. Run server
    ```bash
    sbt run
    ```

Application should be now available here: http://localhost:8080/api/v1   
Docs: http://localhost:8080/docs
   
### How to run tests
```bash
sbt it:test
```