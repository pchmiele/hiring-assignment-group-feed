import Dependencies._

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    organization := "com.pchmiele",
    name := "hiring-assignment-group-feed",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.13.1",
    Defaults.itSettings,
    scalacOptions := Seq(
      "-deprecation",
      "-encoding",
      "UTF-8",
      "-Xfatal-warnings",
      "-feature",
      "-language:implicitConversions",
      "-language:existentials",
      "-language:higherKinds"
    ),
    libraryDependencies ++= Seq(
      AkkaHttp,
      AkkaStream,
      AkkaHttpCirce,
      Postgres,
      PureConfig,
      PureConfigCatsEffect,
      Logback,
      ScalaTest,
      CirceCore,
      CirceParser,
      CirceGeneric,
      CatsCore,
      CatsEffect,
      Doobie,
      DoobiePostgres,
      TestContainers,
      TestContainersPostgres,
      Sttp,
      SttpCats,
      SttpCirce,
      FLyway,
      Tapir,
      TapirAkkaHttp,
      TapirAkkaHttpSwagger,
      TapirDocs,
      TapirOpenApiCirceYaml,
      TapirCirceJson
    ),
    parallelExecution in IntegrationTest := false,
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)
  )