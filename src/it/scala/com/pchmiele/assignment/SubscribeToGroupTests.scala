package com.pchmiele.assignment

import com.softwaremill.sttp.{StatusCodes, sttp}

class SubscribeToGroupTests extends BaseTest {
  "User on POST /api/v1/groups/{groupId}/subscribe" should "be able to subscribe to existing group" in {
    val existingUserId  = "2"
    val existingGroupId = 3L
    val response = subscribeUserToGroup(existingGroupId).auth
      .bearer(existingUserId)
      .send()

    response.code shouldBe StatusCodes.Created
  }

  it should "get InternalServerError when subscribing twice to the same existing group" in {
    val existingUserId  = "2"
    val existingGroupId = 3L
    val response = subscribeUserToGroup(existingGroupId).auth
      .bearer(existingUserId)
      .send()

    response.code shouldBe StatusCodes.InternalServerError
  }

  it should "get InternalServerError when subscribing  to not existing group" in {
    val existingUserId     = "2"
    val notExistingGroupId = 100L
    val response = subscribeUserToGroup(notExistingGroupId).auth
      .bearer(existingUserId)
      .send()

    response.code shouldBe StatusCodes.InternalServerError
  }

  private def subscribeUserToGroup(groupId: GroupId) =
    sttp
      .post(uri(s"/groups/$groupId/subscribe"))

}
