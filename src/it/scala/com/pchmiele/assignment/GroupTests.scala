package com.pchmiele.assignment

import com.pchmiele.assignment.ws.responses.GroupResponse
import com.softwaremill.sttp.circe.asJson
import com.softwaremill.sttp.{StatusCodes, sttp}
import io.circe.generic.auto._

class GroupTests extends BaseTest {
  "Everyone on GET /api/v1/groups/{groupId}" should "be able to see information about particular group" in {
    val existingGroupId = "1"
    val response        = group(existingGroupId).send()
    response.code shouldBe StatusCodes.Ok
  }

  it should "get NotFound not existing group provided" in {
    val notExistingGroup = "100"
    val response         = group(notExistingGroup).send()
    response.code shouldBe StatusCodes.NotFound
  }

  it should "get NotFound wrong type of groupId provided" in {
    val notExistingGroup = "asd"
    val response         = group(notExistingGroup).send()
    response.code shouldBe StatusCodes.NotFound
  }

  private def group(groupId: String) =
    sttp
      .get(uri(s"/groups/$groupId"))
      .response(asJson[GroupResponse])
}
