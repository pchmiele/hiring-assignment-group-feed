package com.pchmiele.assignment

import com.pchmiele.assignment.ws.responses.GroupsResponse
import com.softwaremill.sttp.circe.asJson
import com.softwaremill.sttp.{DeserializationError, sttp}
import io.circe
import io.circe.generic.auto._

class GroupsTests extends BaseTest {
  "Everyone on GET /api/v1/groups" should "be able to see all available groups" in {
    val response = listGroups.send().unsafeBody
    getGroupIds(response) should contain theSameElementsAs (List(1, 2, 3, 4, 5, 6, 7))
  }

  private def getGroupIds(response: Either[DeserializationError[circe.Error], GroupsResponse]): Seq[GroupId] =
    response.map(_.groups).getOrElse(List.empty).map(_.id)

  private val listGroups = sttp
    .get(uri("/groups"))
    .response(asJson[GroupsResponse])
}
