package com.pchmiele.assignment

import com.pchmiele.assignment.ws.responses.GroupsResponse
import com.softwaremill.sttp.circe.asJson
import com.softwaremill.sttp.{StatusCodes, sttp}
import io.circe.generic.auto._

class UserGroupsTests extends BaseTest {
  "User on GET /api/v1/me/groups" should "be able to list groups to which he/she is subscribed" in {
    val existingUserId = "1"
    val response = listUserGroups.auth
      .bearer(existingUserId)
      .response(asJson[GroupsResponse])
      .send()
      .unsafeBody

    val groupIds = response.map(_.groups).getOrElse(List.empty)
    groupIds.map(_.id) should contain theSameElementsAs (List(1, 2, 4))
  }

  it should "get Unauthorized when user does not exists" in {
    val notExistingUserId = "5"
    val response = listUserGroups.auth
      .bearer(notExistingUserId)
      .send()
    response.code shouldBe StatusCodes.Unauthorized
  }

  it should "get Unauthorized when token has a wrong type" in {
    val response = listUserGroups.auth
      .bearer("wrongTokenType")
      .send()
    response.code shouldBe StatusCodes.Unauthorized
  }

  it should "get BadRequest when no token provided" in {
    val response = listUserGroups.send()
    response.code shouldBe StatusCodes.BadRequest
  }

  private val listUserGroups = sttp
    .get(uri("/me/groups"))

}
