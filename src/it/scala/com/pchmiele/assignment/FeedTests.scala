package com.pchmiele.assignment

import com.pchmiele.assignment.ws.responses.FeedResponse
import com.softwaremill.sttp.circe.asJson
import com.softwaremill.sttp.{DeserializationError, Id, Response, StatusCodes, sttp}
import io.circe
import io.circe.generic.auto._

class FeedTests extends BaseTest {

  "User on GET /api/v1/feed" should "be able to see all posts from all feeds he is subscribed to" in {
    val existingUserId = "1"

    val response = listPostsFromAllFeeds(existingUserId)
    response.code shouldBe StatusCodes.Ok
    getPostIds(response) shouldBe List(5, 4, 3, 2, 1)
  }

  it should "get Unauthorized if user does not exist" in {
    val notExistingUserId = "100"

    val response = listPostsFromAllFeeds(notExistingUserId)
    response.code shouldBe StatusCodes.Unauthorized
  }

  it should "get BadRequest if not valid UserId provided" in {
    val notValiUserId = "asdasdk"

    val response = listPostsFromAllFeeds(notValiUserId)
    response.code shouldBe StatusCodes.Unauthorized
  }

  it should "be able to see all posts from all feeds and use pagination" in {
    val existingUserId = "1"

    val response = listPostsFromAllFeeds(existingUserId, None, Some(2))
    response.code shouldBe StatusCodes.Ok
    getPostIds(response) shouldBe List(5, 4)

    val response2 = listPostsFromAllFeeds(existingUserId, Some(4), Some(3))
    response2.code shouldBe StatusCodes.Ok
    getPostIds(response2) shouldBe List(3, 2, 1)

    val response3 = listPostsFromAllFeeds(existingUserId, Some(2), Some(50))
    response3.code shouldBe StatusCodes.Ok
    getPostIds(response3) shouldBe List(1)
  }

  private def getPostIds(response: Id[Response[Either[DeserializationError[circe.Error], FeedResponse]]]) =
    response.unsafeBody.map(_.posts.map(_.postId)).getOrElse(List.empty)

  private def listPostsFromAllFeeds(
      userId: String,
      cursorOpt: Option[Long] = None,
      limitOpt: Option[Int] = None
  ): Id[Response[Either[DeserializationError[circe.Error], FeedResponse]]] = {
    val params = (cursorOpt, limitOpt) match {
      case (Some(cursor), Some(limit)) => s"?cursor=$cursor&limit=$limit"
      case (Some(cursor), None)        => s"?cursor=$cursor"
      case (None, Some(limit))         => s"?limit=$limit"
      case (None, None)                => ""
    }
    sttp
      .get(uri(s"/feed$params"))
      .auth
      .bearer(userId)
      .response(asJson[FeedResponse])
      .send()
  }
}
