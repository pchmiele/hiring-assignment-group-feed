package com.pchmiele.assignment

import com.pchmiele.assignment.ws.requests.PostRequest
import com.softwaremill.sttp.circe._
import com.softwaremill.sttp.{StatusCodes, sttp}
import io.circe.generic.auto._

class AddNewPostTests extends BaseTest {
  "User on POST /api/v1/group/{groupId}/feed" should "be able to create new post to feed of particular group" in {
    val existingGroupId = 1L
    val existingUserId  = "1"

    val response = addNewPostToFeed(existingGroupId)
      .body(PostRequest("content"))
      .auth
      .bearer(existingUserId)
      .send()

    response.code shouldBe StatusCodes.Created
  }

  it should "get Unauthorized when not existing user used" in {
    val existingGroupId = 1L
    val notExistingUser = "100"

    val response = addNewPostToFeed(existingGroupId)
      .body(PostRequest("content"))
      .auth
      .bearer(notExistingUser)
      .send()

    response.code shouldBe StatusCodes.Unauthorized
  }

  it should "get Unauthorized when wrong token type used" in {
    val existingGroupId  = 1L
    val wrongTypeOfToken = "asd"

    val response = addNewPostToFeed(existingGroupId)
      .body(PostRequest("content"))
      .auth
      .bearer(wrongTypeOfToken)
      .send()

    response.code shouldBe StatusCodes.Unauthorized
  }

  it should "get Unauthorized when not existing GroupId used" in {
    val notExistingGroupId = 100L
    val existingUserId     = "1"

    val response = addNewPostToFeed(notExistingGroupId)
      .body(PostRequest("content"))
      .auth
      .bearer(existingUserId)
      .send()

    response.code shouldBe StatusCodes.Unauthorized
  }

  private def addNewPostToFeed(groupId: GroupId) =
    sttp
      .post(uri(s"/groups/$groupId/feed"))

}
