package com.pchmiele.assignment

import com.pchmiele.assignment.ws.responses._
import com.softwaremill.sttp._
import com.softwaremill.sttp.circe._
import io.circe
import io.circe.generic.auto._

class GroupFeedTest extends BaseTest {

  "Everyone on GET /api/v1/groups/{groupId}/feed" should "be able to list all posts in group feed" in {
    val existingGroupId = 1L
    val existingUserId  = "1"

    val response: Id[Response[Either[DeserializationError[circe.Error], FeedResponse]]] =
      listPostsFromFeed(existingGroupId).auth
        .bearer(existingUserId)
        .response(asJson[FeedResponse])
        .send()

    response.code shouldBe StatusCodes.Ok
    getPostIds(response) shouldBe List(5, 4, 1)
  }

  it should "get Unauthorized, when user does not exist" in {
    val existingGroupId   = 1L
    val notExistingUserId = "5"

    val response = listPostsFromFeed(existingGroupId).auth
      .bearer(notExistingUserId)
      .send()

    response.code shouldBe StatusCodes.Unauthorized
  }

  it should "get Unauthorized, when user is not subscribed to the feed" in {
    val notExistingGroupId = 100L
    val existingUserId     = "1"

    val response = listPostsFromFeed(notExistingGroupId).auth
      .bearer(existingUserId)
      .send()

    response.code shouldBe StatusCodes.Unauthorized
  }

  private def getPostIds(
      response: Id[Response[Either[DeserializationError[circe.Error], FeedResponse]]]
  ): List[PostId] =
    response.unsafeBody.map(_.posts.map(_.postId)).getOrElse(List.empty)

  private def listPostsFromFeed(groupId: GroupId) =
    sttp
      .get(uri(s"/groups/$groupId/feed"))

}
