package com.pchmiele.assignment.utils

import com.pchmiele.assignment.config.{Config, DbConfig, WebServerConfig}

trait ConfigValues {
  val user         = "my_user"
  val password     = "my_pass"
  val databaseName = "group_feeds_app"

  def dbConfig(jdbcUrl: String) = DbConfig(user, password, jdbcUrl)

  val apiUri     = "api"
  val apiVersion = "v1"
  val host       = "localhost"
  val port       = 8080
  val webConfig  = WebServerConfig(apiUri = apiUri, apiVersion = apiVersion, host = host, port = port)

  def config(jdbcUrl: String) = Config(dbConfig(jdbcUrl), webConfig)
}
