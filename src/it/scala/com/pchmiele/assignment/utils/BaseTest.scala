package com.pchmiele.assignment

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import cats.effect.IO
import com.dimafeng.testcontainers.{ForAllTestContainer, PostgreSQLContainer}
import com.pchmiele.assignment.db.DbMigration
import com.pchmiele.assignment.utils.ConfigValues
import com.pchmiele.assignment.ws.WebServer
import com.softwaremill.sttp.{HttpURLConnectionBackend, _}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.Future

trait BaseTest extends FlatSpec with ForAllTestContainer with Matchers with ConfigValues {
  implicit val system           = ActorSystem("web-service-actor-system")
  implicit val materializer     = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  implicit val sttpBackend      = HttpURLConnectionBackend()

  override val container = PostgreSQLContainer(
    dockerImageNameOverride = "postgres:12.0-alpine",
    databaseName = databaseName,
    username = user,
    password = password
  )

  var bindingFuture: Future[Http.ServerBinding] = _

  override def beforeStop(): Unit = {
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }

  override def afterStart(): Unit = {
    implicit val cs = IO.contextShift(ExecutionContexts.synchronous)
    val transactor =
      Transactor
        .fromDriverManager[IO]("org.postgresql.Driver", container.jdbcUrl, container.username, container.password)

    DbMigration.migrateDb(dbConfig(container.jdbcUrl)).unsafeRunSync()
    bindingFuture = WebServer.start(webConfig, transactor)
  }

  def uri(path: String): Uri = {
    val stringUri = s"http://$host:$port/$apiUri/$apiVersion$path"
    uri"$stringUri"
  }
}
