package com.pchmiele.assignment

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import cats.effect.{ExitCode, IO, IOApp}
import com.pchmiele.assignment.config.Config
import com.pchmiele.assignment.db.DbMigration
import com.pchmiele.assignment.log._
import com.pchmiele.assignment.ws.WebServer
import doobie.util.transactor.Transactor

import scala.io.StdIn

object Main extends IOApp {
  implicit val system       = ActorSystem("web-service-actor-system")
  implicit val materializer = ActorMaterializer()

  def onError(ex: Throwable): IO[ExitCode] =
    for {
      _ <- error(s"Could not start server, because of the exception: ${ex.getMessage}")
      _ <- IO(ex.printStackTrace())
    } yield ExitCode.Error

  def onClose(exitCode: ExitCode): IO[ExitCode] =
    for {
      _ <- IO.fromFuture(IO(system.terminate()))
      _ <- info(s"Application successfully terminated.")
    } yield exitCode

  def startServer(config: Config, transactor: Transactor[IO]): IO[ExitCode] =
    for {
      webServer <- IO.fromFuture(IO(WebServer.start(config.server, transactor)))
      _         <- info(s"Server online at http://${config.server.host}:${config.server.port}")
      _         <- info(s"Press RETURN to stop...")
      _         <- IO(StdIn.readLine())
      _         <- IO.fromFuture(IO(webServer.unbind()))
    } yield ExitCode.Success

  override def run(args: List[String]): IO[ExitCode] = {
    (for {
      config <- Config.loadConfig()
      transactor = Transactor.fromDriverManager[IO](
        "org.postgresql.Driver",
        config.db.jdbcUrl,
        config.db.user,
        config.db.password
      )
      _      <- DbMigration.migrateDb(config.db)
      status <- startServer(config, transactor)
    } yield status)
      .handleErrorWith { onError }
      .flatMap { onClose }
  }
}
