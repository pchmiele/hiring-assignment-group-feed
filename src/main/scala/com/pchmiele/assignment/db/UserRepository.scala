package com.pchmiele.assignment.db

import cats.effect.Sync
import com.pchmiele.assignment.UserId
import doobie.util.transactor.Transactor
import doobie.implicits._

trait UserRepository[F[_]] {
  def exists(userId: UserId): F[Boolean]
}

class UserRepositoryImpl[F[_]: Sync](transactor: Transactor[F]) extends UserRepository[F] {
  override def exists(userId: UserId): F[Boolean] =
    sql"SELECT COUNT(1) FROM users WHERE id = $userId"
      .query[Boolean]
      .unique
      .transact(transactor)
}
