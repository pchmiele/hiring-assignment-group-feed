package com.pchmiele.assignment.db

import cats.effect.IO
import com.pchmiele.assignment.config.DbConfig
import org.flywaydb.core.Flyway

object DbMigration {
  def migrateDb(dbConfig: DbConfig): IO[Int] =
    for {
      flyway <- IO(Flyway.configure.dataSource(dbConfig.jdbcUrl, dbConfig.user, dbConfig.password).load)
      result <- IO(flyway.migrate())
    } yield result
}
