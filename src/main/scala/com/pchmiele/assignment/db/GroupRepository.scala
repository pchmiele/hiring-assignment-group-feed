package com.pchmiele.assignment.db

import cats.effect.Sync
import com.pchmiele.assignment.GroupId
import com.pchmiele.assignment.domain.Group
import doobie.util.transactor.Transactor
import doobie.implicits._

trait GroupRepository[F[_]] {
  def list(): F[List[Group]]
  def get(groupId: GroupId): F[Option[Group]]
}

class GroupRepositoryImpl[F[_]: Sync](transactor: Transactor[F]) extends GroupRepository[F] {
  override def list(): F[List[Group]] =
    sql"SELECT * FROM groups"
      .query[Group]
      .stream
      .compile
      .toList
      .transact(transactor)

  override def get(groupId: GroupId): F[Option[Group]] =
    sql"SELECT * FROM groups WHERE id = $groupId "
      .query[Group]
      .option
      .transact(transactor)
}
