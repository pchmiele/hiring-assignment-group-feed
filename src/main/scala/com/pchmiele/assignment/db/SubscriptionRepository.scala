package com.pchmiele.assignment.db

import cats.effect.Sync
import com.pchmiele.assignment.domain.Subscription
import com.pchmiele.assignment.{GroupId, UserId}
import doobie.util.transactor.Transactor
import doobie._
import doobie.implicits._

trait SubscriptionRepository[F[_]] {
  def list(userId: UserId): F[List[Subscription]]
  def exists(userId: UserId, groupId: GroupId): F[Boolean]
  def insert(userId: UserId, groupId: GroupId): F[Int]
}

class SubscriptionRepositoryImpl[F[_]: Sync](transactor: Transactor[F]) extends SubscriptionRepository[F] {
  override def list(userId: UserId): F[List[Subscription]] =
    sql"SELECT * FROM subscriptions WHERE user_id = $userId"
      .query[Subscription]
      .stream
      .compile
      .toList
      .transact(transactor)

  override def insert(userId: UserId, groupId: GroupId): F[Int] =
    sql"INSERT INTO subscriptions (user_id, group_id) VALUES ($userId, $groupId)".update.run
      .exceptSql {
        case _: org.postgresql.util.PSQLException => Sync[ConnectionIO].pure(0)
      }
      .transact(transactor)

  override def exists(userId: UserId, groupId: GroupId): F[Boolean] =
    sql"SELECT COUNT(1) FROM subscriptions WHERE user_id = $userId AND group_id = $groupId"
      .query[Boolean]
      .unique
      .transact(transactor)
}
