package com.pchmiele.assignment.db

import java.sql.Timestamp

import cats.effect.Sync
import com.pchmiele.assignment.domain.Post
import com.pchmiele.assignment.{GroupId, UserId}
import doobie.util.transactor.Transactor
import doobie.implicits._

trait PostRepository[F[_]] {
  def insert(userId: UserId, groupId: GroupId, content: String): F[Int]
  def selectWhere(userId: UserId, groupId: GroupId): F[List[Post]]
  def selectWhere(userId: UserId, cursorOpt: Option[Long], limit: Int): F[List[Post]]
}

class PostRepositoryImpl[F[_]: Sync](transactor: Transactor[F]) extends PostRepository[F] {
  def insert(userId: UserId, groupId: GroupId, content: String): F[Int] =
    sql"""INSERT INTO posts(content, user_id, group_id)
         |SELECT $content, $userId, $groupId
         |WHERE exists (
         |  select 1 from subscriptions where user_id = $userId
         |)""".stripMargin.update.run
      .transact(transactor)

  override def selectWhere(userId: UserId, groupId: GroupId): F[List[Post]] =
    sql"""SELECT p.id, p.content, p.user_id, u.user_name, p.created_at
          |FROM posts p
          |INNER JOIN subscriptions s ON p.group_id = s.group_id
          |INNER JOIN users u ON s.user_id = u.id
          |WHERE p.group_id = $groupId AND s.user_id = $userId
          |ORDER BY p.id DESC;""".stripMargin
      .query[(Long, String, Long, String, Timestamp)]
      .map { case (id, content, userId, userName, createdAt) => Post(id, content, userId, userName, createdAt) }
      .stream
      .compile
      .toList
      .transact(transactor)

  override def selectWhere(userId: UserId, cursorOpt: Option[Long], limit: Int): F[List[Post]] = {

    val selectWithJoins = fr"""SELECT p.id, p.content, p.user_id, u.user_name, p.created_at, p.group_id
         |FROM posts p
         |INNER JOIN subscriptions s ON p.group_id = s.group_id
         |INNER JOIN users u ON s.user_id = u.id""".stripMargin

    val filter = cursorOpt match {
      case Some(cursor) => fr"""WHERE s.user_id = $userId AND p.id < $cursor"""
      case None         => fr"""WHERE s.user_id = $userId"""
    }

    val sortAndLimit =
      fr"""ORDER BY p.id DESC
          |LIMIT $limit""".stripMargin

    (selectWithJoins ++ filter ++ sortAndLimit)
      .query[(Long, String, Long, String, Timestamp, Long)]
      .map {
        case (id, content, userId, userName, createdAt, groupId) =>
          Post(id, content, userId, userName, createdAt, Some(groupId))
      }
      .stream
      .compile
      .toList
      .transact(transactor)
  }
}
