package com.pchmiele.assignment.services

import com.pchmiele.assignment.{GroupId, UserId}
import com.pchmiele.assignment.db.{GroupRepository, PostRepository}
import com.pchmiele.assignment.domain.{Group, Post}

trait GroupService[F[_]] {
  def listGroups(): F[List[Group]]
  def find(groupId: GroupId): F[Option[Group]]
  def addNewPost(userId: UserId, groupId: GroupId, content: String): F[Int]
  def feed(userId: UserId, groupId: GroupId): F[List[Post]]
}

class GroupServiceImpl[F[_]](groupRepository: GroupRepository[F], postRepository: PostRepository[F])
    extends GroupService[F] {
  override def listGroups(): F[List[Group]] =
    groupRepository.list()

  override def find(groupId: GroupId): F[Option[Group]] =
    groupRepository.get(groupId)

  override def addNewPost(userId: UserId, groupId: GroupId, content: String): F[Int] =
    postRepository.insert(userId, groupId, content)

  override def feed(userId: UserId, groupId: GroupId): F[List[Post]] =
    postRepository.selectWhere(userId, groupId)

}
