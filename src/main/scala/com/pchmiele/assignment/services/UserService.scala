package com.pchmiele.assignment.services

import cats.Monad
import cats.implicits._
import com.pchmiele.assignment.{GroupId, UserId}
import com.pchmiele.assignment.db.{PostRepository, SubscriptionRepository, UserRepository}
import com.pchmiele.assignment.domain.{Group, Post}

trait UserService[F[_]] {
  def showAllPostsFeed(userId: UserId, cursor: Option[Long], limit: Int): F[List[Post]]
  def listGroups(userId: UserId): F[List[Group]]
  def exists(userId: UserId): F[Boolean]
  def subscribe(userId: UserId, groupId: GroupId): F[Int]
}

class UserServiceImpl[F[_]: Monad](
    subscriptionRepository: SubscriptionRepository[F],
    userRepository: UserRepository[F],
    postRepository: PostRepository[F]
) extends UserService[F] {
  override def showAllPostsFeed(userId: UserId, cursor: Option[Long], limit: Int): F[List[Post]] =
    postRepository.selectWhere(userId, cursor, limit)

  override def listGroups(userId: UserId): F[List[Group]] =
    for {
      subscriptions <- subscriptionRepository.list(userId)
      groups = subscriptions.map(s => Group(s.groupId))
    } yield groups

  override def exists(userId: UserId): F[Boolean] =
    userRepository.exists(userId)

  override def subscribe(userId: UserId, groupId: GroupId): F[Int] =
    subscriptionRepository
      .insert(userId, groupId)
}
