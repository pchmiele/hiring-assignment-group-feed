package com.pchmiele.assignment.services

import com.pchmiele.assignment.{GroupId, UserId}
import com.pchmiele.assignment.db.SubscriptionRepository

trait SubscriptionService[F[_]] {
  def exists(userId: UserId, groupId: GroupId): F[Boolean]
}

class SubscriptionServiceImpl[F[_]](subscriptionRepository: SubscriptionRepository[F]) extends SubscriptionService[F] {
  override def exists(userId: UserId, groupId: GroupId): F[Boolean] =
    subscriptionRepository.exists(userId, groupId)
}
