package com.pchmiele.assignment.config

import cats.effect.IO
import pureconfig.ConfigSource
import pureconfig.generic.auto._
import pureconfig.module.catseffect._

case class DbConfig(user: String, password: String, jdbcUrl: String)
case class WebServerConfig(apiUri: String, apiVersion: String, host: String, port: Int)
case class Config(db: DbConfig, server: WebServerConfig)

object Config {
  def loadConfig(): IO[Config] = ConfigSource.default.loadF[IO, Config]
}
