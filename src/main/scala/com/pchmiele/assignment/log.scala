package com.pchmiele.assignment

import cats.effect.IO
import org.slf4j.LoggerFactory

object log {

  private val logger = LoggerFactory.getLogger("hiring-assignment")

  def warn(msg: => String, ex: => Option[Throwable] = None): IO[Unit] =
    IO.delay(ex.fold(logger.warn(msg))(err => logger.warn(msg, err)))

  def info(msg: => String): IO[Unit] =
    IO.delay(logger.info(msg))

  def debug(msg: => String): IO[Unit] =
    IO.delay(logger.debug(msg))

  def error(msg: => String, ex: => Option[Throwable] = None): IO[Unit] =
    IO.delay(ex.fold(logger.error(msg))(err => logger.error(msg, err)))
}
