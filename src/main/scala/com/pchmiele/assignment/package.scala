package com.pchmiele

package object assignment {
  type GroupId        = Long
  type UserId         = Long
  type SubscriptionId = Long
  type PostId         = Long
}
