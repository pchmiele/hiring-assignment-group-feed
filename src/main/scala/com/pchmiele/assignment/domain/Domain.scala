package com.pchmiele.assignment.domain

import java.sql.Timestamp
import com.pchmiele.assignment._

case class Group(id: GroupId)
case class Subscription(subscriptionId: SubscriptionId, userId: UserId, groupId: GroupId)

case class Post(
    postId: SubscriptionId,
    content: String,
    userId: UserId,
    userName: String,
    createdAt: Timestamp,
    groupId: Option[GroupId] = None
)
