package com.pchmiele.assignment.ws

import cats.implicits._
import cats.{Applicative, Monad}
import com.pchmiele.assignment.services.UserService
import com.pchmiele.assignment.ws.errors._

trait Authenticator[F[_]] {
  def authenticate(userId: String): F[Either[ErrorInfo, Long]]
}

class AuthenticatorImpl[F[_]: Applicative: Monad](userService: UserService[F]) extends Authenticator[F] {
  override def authenticate(userId: String): F[Either[ErrorInfo, Long]] =
    userId match {
      case id if id.toLongOption.isDefined =>
        userService
          .exists(id.toLong)
          .map {
            case true  => Right(id.toLong)
            case false => Left(Unauthorized("User does not exist"))
          }
      case _ =>
        implicitly[Monad[F]].pure(Left(Unauthorized("Not valid token. Hint - it should be existing user id (Long)")))
    }
}
