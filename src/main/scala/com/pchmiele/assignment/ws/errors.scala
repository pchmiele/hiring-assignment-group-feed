package com.pchmiele.assignment.ws

package object errors {
  sealed trait ErrorInfo                         extends Product with Serializable
  case class NotFound(what: String)              extends ErrorInfo
  case class Unauthorized(reason: String)        extends ErrorInfo
  case class BadRequest(reason: String)          extends ErrorInfo
  case class InternalServerError(reason: String) extends ErrorInfo
  case class Unknown(code: Int, msg: String)     extends ErrorInfo
}
