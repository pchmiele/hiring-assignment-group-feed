package com.pchmiele.assignment.ws

package object requests {
  case class PostRequest(content: String)
}
