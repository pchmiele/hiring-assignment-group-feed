package com.pchmiele.assignment.ws

import java.sql.Timestamp
import java.time.Instant

import com.pchmiele.assignment.domain
import tapir.{auth, jsonBody, oneOf, _}
import com.pchmiele.assignment.ws.endpoints.BaseEndpoint._
import com.pchmiele.assignment.ws.responses.{AllFeedsResponse, FeedResponse}
import io.circe.generic.auto._
import tapir.json.circe._

object FeedEndpoints {
  private val limitParameter =
    query[Option[Int]]("limit")
      .description("Maximum number of feeds to retrieve. By default: 10")
      .validate(Validator.min(1).asOptionElement)
      .validate(Validator.max(100).asOptionElement)

  private val cursor =
    query[Option[Long]]("cursor")
      .description("Index of the last element")
      .validate(Validator.min(0L).asOptionElement)

  val allFeedResponse = jsonBody[FeedResponse]
    .example(
      AllFeedsResponse(
        List(
          domain.Post(
            postId = 1L,
            content = "Lorem ipsum",
            userId = 1L,
            userName = "Adam",
            createdAt = new Timestamp(Instant.now().toEpochMilli),
            groupId = Some(1L)
          )
        )
      )
    )

  val listPostFromALlFeeds = baseEndpoint.get
    .in("feed")
    .in(cursor.and(limitParameter))
    .description("Feed of all posts from all groups user is subscribed to")
    .in(auth.bearer)
    .out(allFeedResponse)
    .errorOut(
      oneOf(
        unauthorized,
        unknown
      )
    )
}
