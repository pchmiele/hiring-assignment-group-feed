package com.pchmiele.assignment.ws.endpoints

import java.sql.Timestamp
import java.time.Instant

import com.pchmiele.assignment.domain
import com.pchmiele.assignment.ws.endpoints.BaseEndpoint._
import com.pchmiele.assignment.ws.requests.PostRequest
import com.pchmiele.assignment.ws.responses.{FeedResponse, GroupFeedResponse, GroupsResponse}
import io.circe.generic.auto._
import tapir.json.circe._
import tapir.model.StatusCodes
import tapir.{jsonBody, _}

object GroupEndpoints {
  private val groupIdPath =
    path[Long]("groupId")
      .description("Id of group")
      .validate(Validator.min(0))

  val findGroup = baseEndpoint.get
    .in("groups" / groupIdPath)
    .description("Description of particular group")
    .out(groupResponse)
    .errorOut(
      oneOf(
        notFound,
        badRequest,
        unknown
      )
    )

  val groupsResponse =
    jsonBody[GroupsResponse].example(GroupsResponse(List(domain.Group(1L), domain.Group(2L), domain.Group(3L))))

  val listGroups = baseEndpoint.get
    .in("groups")
    .description("Lists all available groups")
    .out(groupsResponse)

  val groupFeedResponse = jsonBody[FeedResponse]
    .example(
      GroupFeedResponse(
        List(
          domain.Post(
            postId = 1L,
            content = "Lorem ipsum",
            userId = 1L,
            userName = "Adam",
            createdAt = new Timestamp(Instant.now().toEpochMilli),
            groupId = None
          )
        ),
        1L
      )
    )

  val listPostsFromGroupFeed = baseEndpoint.get
    .in("groups" / groupIdPath / "feed")
    .in(auth.bearer)
    .description("List all posts from group feed sorted by creation date (latest first)")
    .out(groupFeedResponse)
    .errorOut(
      oneOf(
        notFound,
        unauthorized,
        badRequest,
        unknown
      )
    )

  val addPostToGroupFeed = baseEndpoint.post
    .in("groups" / path[Long]("groupId") / "feed")
    .in(
      jsonBody[PostRequest]
        .description("Post to add to feed")
        .example(PostRequest("Lorem ipsum"))
    )
    .in(auth.bearer)
    .description("Add new post to group feed")
    .out(statusCode(StatusCodes.Created))
    .errorOut(
      oneOf(
        notFound,
        unauthorized,
        badRequest,
        internalServerError,
        unknown
      )
    )

  val subscribeToGroup = baseEndpoint.post
    .in("groups" / path[Long]("groupId") / "subscribe")
    .description("Subscribe to the group")
    .in(auth.bearer)
    .out(statusCode(StatusCodes.Created))
    .errorOut(
      oneOf(
        notFound,
        unauthorized,
        badRequest,
        internalServerError,
        unknown
      )
    )
}
