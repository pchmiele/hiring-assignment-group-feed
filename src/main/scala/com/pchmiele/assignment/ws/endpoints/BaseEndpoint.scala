package com.pchmiele.assignment.ws.endpoints

import com.pchmiele.assignment.ws.errors._
import com.pchmiele.assignment.ws.responses.GroupResponse
import io.circe.generic.auto._
import tapir._
import tapir.json.circe._
import tapir.model.StatusCodes

object BaseEndpoint {
  val baseEndpoint =
    endpoint
      .in("api" / "v1")

  val groupResponse =
    jsonBody[GroupResponse].example(GroupResponse(1L))

  val unauthorized = statusMapping(StatusCodes.Unauthorized, jsonBody[Unauthorized].description("Unauthorized"))
  val unknown      = statusDefaultMapping(jsonBody[Unknown].description("Unknown"))
  val notFound     = statusMapping(StatusCodes.NotFound, jsonBody[NotFound].description("Group not found"))
  val badRequest   = statusMapping(StatusCodes.BadRequest, jsonBody[BadRequest].description("Bad Request"))
  val internalServerError = statusMapping(
    StatusCodes.InternalServerError,
    jsonBody[InternalServerError].description("Internal Server Error")
  )

}
