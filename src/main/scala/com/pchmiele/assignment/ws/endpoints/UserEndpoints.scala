package com.pchmiele.assignment.ws.endpoints

import com.pchmiele.assignment.domain
import tapir.{auth, jsonBody, oneOf, _}
import com.pchmiele.assignment.ws.endpoints.BaseEndpoint._
import com.pchmiele.assignment.ws.responses.{GroupsResponse, MyGroupsResponse}
import tapir.json.circe._
import io.circe.generic.auto._

object UserEndpoints {
  val myGroupsResponse =
    jsonBody[GroupsResponse].example(MyGroupsResponse(List(domain.Group(1L), domain.Group(2L), domain.Group(3L))))

  val listsUserGroups = baseEndpoint.get
    .in("me" / "groups")
    .description("Lists all groups user belongs to")
    .in(auth.bearer)
    .out(myGroupsResponse)
    .errorOut(
      oneOf(
        unauthorized,
        unknown
      )
    )
}
