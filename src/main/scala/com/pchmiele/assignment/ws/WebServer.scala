package com.pchmiele.assignment.ws

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import cats.arrow.FunctionK
import cats.effect.IO
import com.pchmiele.assignment.config.WebServerConfig
import com.pchmiele.assignment.db.{
  GroupRepositoryImpl,
  PostRepositoryImpl,
  SubscriptionRepositoryImpl,
  UserRepositoryImpl
}
import com.pchmiele.assignment.services.{GroupServiceImpl, SubscriptionServiceImpl, UserServiceImpl}
import com.pchmiele.assignment.ws.routes.{DocRoutes, FeedRoutes, GroupRoutes, UserRoutes}
import doobie.util.transactor.Transactor

import scala.concurrent.Future

object WebServer {
  def start(config: WebServerConfig, transactor: Transactor[IO])(
      implicit actorSystem: ActorSystem,
      materializer: ActorMaterializer
  ): Future[Http.ServerBinding] = {
    import config._

    implicit val ioToFuture: FunctionK[IO, Future] = λ[FunctionK[IO, Future]](_.unsafeToFuture)

    val subscriptionRepository = new SubscriptionRepositoryImpl(transactor)
    val subscriptionService    = new SubscriptionServiceImpl(subscriptionRepository)

    val postRepository = new PostRepositoryImpl(transactor)
    val userRepository = new UserRepositoryImpl(transactor)
    val userService    = new UserServiceImpl(subscriptionRepository, userRepository, postRepository)
    val authenticator  = new AuthenticatorImpl(userService)

    val groupRepository = new GroupRepositoryImpl(transactor)
    val groupService    = new GroupServiceImpl(groupRepository, postRepository)
    val groupRoutes     = new GroupRoutes(groupService, userService, authenticator, subscriptionService)
    val feedRoutes      = new FeedRoutes(userService, authenticator)

    val userRoutes = new UserRoutes(userService, authenticator)

    val routes = ignoreTrailingSlash {
      concat(
        DocRoutes.docsRoute,
        feedRoutes.routes,
        groupRoutes.listGroups,
        groupRoutes.findGroup,
        groupRoutes.listPostsFromGroupFeed,
        groupRoutes.addPostToGroupFeed,
        groupRoutes.subscribeToGroup,
        userRoutes.listsUserGroups
      )
    }

    Http()
      .bindAndHandle(routes, host, port)
  }
}
