package com.pchmiele.assignment.ws

import com.pchmiele.assignment.{GroupId, PostId, UserId, domain}

package object responses {
  case class Link(href: String, rel: String, `type`: String)

  case class GroupsResponse(groups: List[GroupResponse], links: List[Link])
  object MyGroupsResponse {
    def apply(groups: List[domain.Group]): GroupsResponse =
      GroupsResponse(groups.map(group => GroupResponse(group.id)), List(Link(Urls.myGroups, "self", "GET")))
  }

  object GroupsResponse {
    def apply(groups: List[domain.Group]): GroupsResponse =
      GroupsResponse(groups.map(group => GroupResponse(group.id)), List(Link(Urls.groups, "self", "GET")))
  }

  case class GroupResponse(id: GroupId, links: List[Link])
  object GroupResponse {
    def apply(group: domain.Group): GroupResponse =
      GroupResponse.apply(group.id)

    def apply(id: GroupId): GroupResponse =
      GroupResponse(
        id,
        List(
          Link(Urls.group(id.toString), "self", "GET"),
          Link(Urls.subscribe(id.toString), "subscribe", "POST"),
          Link(Urls.groupFeed(id.toString), "feed", "GET")
        )
      )
  }

  case class PostResponse(
      postId: PostId,
      content: String,
      userId: UserId,
      userName: String,
      createdAt: String,
      group: Option[GroupResponse]
  )
  object Post {
    def apply(post: domain.Post): PostResponse =
      PostResponse(
        post.postId,
        post.content,
        post.userId,
        post.userName,
        post.createdAt.toString,
        post.groupId.map(GroupResponse(_))
      )
  }

  case class FeedResponse(posts: List[PostResponse], links: List[Link])
  object AllFeedsResponse {
    def apply(posts: List[domain.Post]): FeedResponse = {
      val postResponses = posts.map(Post(_))
      val links = if (posts.isEmpty) {
        List(
          Link(Urls.feed, "self", "GET")
        )
      } else {
        List(
          Link(Urls.feed, "self", "GET"),
          Link(Urls.nextFeed(posts.sortWith(_.postId < _.postId).head.postId), "next", "GET"),
        )
      }

      FeedResponse(
        postResponses,
        links
      )
    }
  }

  object GroupFeedResponse {
    def apply(posts: List[domain.Post], groupId: GroupId): FeedResponse = {
      val postResponses = posts.map(Post(_))
      val links = List(
        Link(Urls.groupFeed(groupId.toString), "self", "GET")
      )

      FeedResponse(
        postResponses,
        links
      )
    }
  }
}
