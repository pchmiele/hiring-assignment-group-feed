package com.pchmiele.assignment.ws

object Urls {
  val api                        = "/api/v1"
  val feed                       = s"$api/feed"
  def nextFeed(cursor: Long)     = s"$feed?cursor=$cursor&limit=10"
  def groups                     = s"$api/groups"
  def group(groupId: String)     = s"$api/groups/$groupId"
  def subscribe(groupId: String) = s"$api/groups/$groupId/subscribe"
  def groupFeed(groupId: String) = s"$api/groups/$groupId/feed"
  val myGroups                   = s"$api/me/groups"
}
