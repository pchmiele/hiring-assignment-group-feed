package com.pchmiele.assignment.ws.routes

import cats.arrow.FunctionK
import cats.effect.Effect
import cats.implicits._
import com.pchmiele.assignment.services.{GroupService, SubscriptionServiceImpl, UserService}
import com.pchmiele.assignment.ws.Authenticator
import com.pchmiele.assignment.ws.endpoints.GroupEndpoints
import com.pchmiele.assignment.ws.errors.{ErrorInfo, InternalServerError, NotFound, Unauthorized}
import com.pchmiele.assignment.ws.responses._
import com.pchmiele.assignment.{GroupId, UserId}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import tapir.server.akkahttp._

import scala.concurrent.Future

class GroupRoutes[F[_]: Effect](
    groupService: GroupService[F],
    userService: UserService[F],
    authenticator: Authenticator[F],
    subscriptionService: SubscriptionServiceImpl[F]
)(implicit NT: FunctionK[F, Future])
    extends FailFastCirceSupport {

  val findGroup = GroupEndpoints.findGroup.toRoute {
    case groupId =>
      val result = groupService
        .find(groupId)
        .map {
          case Some(group) => GroupResponse(group).asRight[ErrorInfo]
          case None        => NotFound(s"Could not find group with id == $groupId ").asLeft[GroupResponse]
        }

      NT(result)
  }

  val listGroups = GroupEndpoints.listGroups.toRoute { _ =>
    val result = groupService
      .listGroups()
      .map(GroupsResponse(_).asRight[Unit])
    NT(result)

  }

  private def listPosts(userId: UserId, groupId: GroupId): F[Either[ErrorInfo, FeedResponse]] =
    groupService.feed(userId, groupId).map(GroupFeedResponse(_, groupId).asRight[ErrorInfo])

  private def listPostsIfAuthorized(
      isAuthorized: Boolean,
      userId: UserId,
      groupId: GroupId
  ): F[Either[ErrorInfo, FeedResponse]] =
    if (isAuthorized) {
      listPosts(userId, groupId)
    } else {
      Effect[F].pure(Unauthorized("User is not a member of this group").asLeft[FeedResponse])
    }

  val listPostsFromGroupFeed = GroupEndpoints.listPostsFromGroupFeed.toRoute {
    case (groupId, userId) =>
      val result = authenticator
        .authenticate(userId)
        .flatMap {
          case Right(id) =>
            for {
              userIsSubscribedToGroup <- subscriptionService.exists(id, groupId)
              posts                   <- listPostsIfAuthorized(userIsSubscribedToGroup, id, groupId)
            } yield posts
          case Left(error) => Effect[F].pure(error.asLeft[FeedResponse])
        }
      NT(result)
  }

  private def addNewPost(userId: UserId, groupId: GroupId, content: String): F[Either[ErrorInfo, Unit]] =
    groupService.addNewPost(userId, groupId, content).map {
      case 1 => ().asRight[ErrorInfo]
      case _ =>
        InternalServerError("Post was not added because of internal server error")
          .asLeft[Unit]
    }

  private def addNewPostIfAuthorized(
      isAuthorized: Boolean,
      userId: UserId,
      groupId: GroupId,
      content: String
  ): F[Either[ErrorInfo, Unit]] =
    if (isAuthorized) {
      addNewPost(userId, groupId, content)
    } else {
      Effect[F].pure(Unauthorized("User is not a member of this group").asLeft[Unit])
    }

  val addPostToGroupFeed = GroupEndpoints.addPostToGroupFeed.toRoute {
    case (groupId, postRequest, userId) =>
      val result = authenticator
        .authenticate(userId)
        .flatMap {
          case Right(id) =>
            for {
              userIsSubscribedToGroup <- subscriptionService.exists(id, groupId)
              result                  <- addNewPostIfAuthorized(userIsSubscribedToGroup, id, groupId, postRequest.content)
            } yield result
          case Left(error) => Effect[F].pure(error.asLeft[Unit])
        }
      NT(result)
  }

  val subscribeToGroup = GroupEndpoints.subscribeToGroup.toRoute {
    case (groupId, userId) =>
      val result =
        authenticator
          .authenticate(userId)
          .flatMap {
            case Right(id) =>
              userService.subscribe(id, groupId).map {
                case 1 => ().asRight[ErrorInfo]
                case _ =>
                  InternalServerError(
                    s"User: $userId was not able to subscribe to ${groupId}"
                  ).asLeft[Unit]
              }
            case Left(error) => Effect[F].pure(error.asLeft[Unit])
          }
      NT(result)
  }
}
