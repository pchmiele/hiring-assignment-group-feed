package com.pchmiele.assignment.ws.routes

import cats.arrow.FunctionK
import cats.effect.Effect
import cats.implicits._
import com.pchmiele.assignment.UserId
import com.pchmiele.assignment.services.UserService
import com.pchmiele.assignment.ws.Authenticator
import com.pchmiele.assignment.ws.endpoints.UserEndpoints
import com.pchmiele.assignment.ws.errors.ErrorInfo
import com.pchmiele.assignment.ws.responses._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import tapir.server.akkahttp._

import scala.concurrent.Future

class UserRoutes[F[_]: Effect](userService: UserService[F], authenticator: Authenticator[F])(
    implicit NT: FunctionK[F, Future]
) extends FailFastCirceSupport {
  def listGroups(userId: UserId): F[Either[ErrorInfo, GroupsResponse]] =
    userService
      .listGroups(userId)
      .map(MyGroupsResponse(_).asRight[ErrorInfo])

  val listsUserGroups = UserEndpoints.listsUserGroups.toRoute {
    case userId =>
      val result = authenticator
        .authenticate(userId)
        .flatMap {
          case Right(id)   => listGroups(id)
          case Left(error) => Effect[F].pure(error.asLeft[GroupsResponse])
        }
      NT(result)
  }
}
