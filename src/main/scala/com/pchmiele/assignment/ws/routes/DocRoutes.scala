package com.pchmiele.assignment.ws.routes

import akka.http.scaladsl.server.Route
import com.pchmiele.assignment.ws.FeedEndpoints
import com.pchmiele.assignment.ws.endpoints.{GroupEndpoints, UserEndpoints}
import tapir.swagger.akkahttp.SwaggerAkka
import tapir.docs.openapi._
import tapir.openapi.circe.yaml._

object DocRoutes {
  def docsRoute: Route = {
    val endpoints = List(
      FeedEndpoints.listPostFromALlFeeds,
      GroupEndpoints.findGroup,
      GroupEndpoints.listGroups,
      GroupEndpoints.listPostsFromGroupFeed,
      GroupEndpoints.addPostToGroupFeed,
      GroupEndpoints.subscribeToGroup,
      UserEndpoints.listsUserGroups
    )
    val doc = endpoints.toOpenAPI("Hiring Assignment Group Feeds - REST Api documentation", "1.0.0").toYaml
    new SwaggerAkka(doc).routes
  }
}
