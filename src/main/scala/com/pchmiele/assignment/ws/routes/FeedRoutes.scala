package com.pchmiele.assignment.ws.routes

import cats.arrow.FunctionK
import cats.effect.Effect
import cats.implicits._
import com.pchmiele.assignment.services.UserService
import com.pchmiele.assignment.ws.errors._
import com.pchmiele.assignment.ws.responses._
import com.pchmiele.assignment.ws.{Authenticator, FeedEndpoints}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import tapir.server.akkahttp._

import scala.concurrent.Future

class FeedRoutes[F[_]: Effect](userService: UserService[F], authenticator: Authenticator[F])(
    implicit NT: FunctionK[F, Future]
) extends FailFastCirceSupport {
  private def handleAuthenticationError(unauthorized: ErrorInfo): F[Either[ErrorInfo, FeedResponse]] =
    Effect[F].pure(unauthorized.asLeft[FeedResponse])

  private def showAllPostsFeed(id: Long, cursor: Option[Long], limit: Int): F[Either[ErrorInfo, FeedResponse]] =
    userService
      .showAllPostsFeed(id, cursor, limit)
      .map(AllFeedsResponse(_).asRight[ErrorInfo])

  val routes =
    FeedEndpoints.listPostFromALlFeeds.toRoute {
      case (cursor, limit, userId) =>
        val result =
          authenticator
            .authenticate(userId)
            .flatMap {
              case Right(id)          => showAllPostsFeed(id, cursor, limit.getOrElse(10))
              case Left(unauthorized) => handleAuthenticationError(unauthorized)
            }
        NT(result)
    }
}
