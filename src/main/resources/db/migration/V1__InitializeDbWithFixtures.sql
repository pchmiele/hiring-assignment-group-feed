CREATE TABLE groups (
  id BIGSERIAL PRIMARY KEY
);

CREATE TABLE users (
 id BIGSERIAL PRIMARY KEY,
 user_name text NOT NULL
);

CREATE TABLE posts (
  id BIGSERIAL PRIMARY KEY,
  content text NOT NULL,
  user_id BIGINT NOT NULL REFERENCES users (id),
  created_at timestamp NOT NULL DEFAULT NOW(),
  group_id BIGINT NOT NULL REFERENCES groups (id)
);

CREATE INDEX posts_user_id ON posts(user_id);
CREATE INDEX posts_group_id ON posts(group_id);

CREATE TABLE subscriptions (
  id BIGSERIAL PRIMARY KEY,
  user_id BIGINT NOT NULL REFERENCES users (id),
  group_id BIGINT NOT NULL REFERENCES groups (id),
  UNIQUE(user_id, group_id)
);

CREATE INDEX subscriptions_user_id ON subscriptions(user_id);
CREATE INDEX subscriptions_group_id ON subscriptions(group_id);

INSERT INTO groups DEFAULT VALUES;
INSERT INTO groups DEFAULT VALUES;
INSERT INTO groups DEFAULT VALUES;
INSERT INTO groups DEFAULT VALUES;
INSERT INTO groups DEFAULT VALUES;
INSERT INTO groups DEFAULT VALUES;
INSERT INTO groups DEFAULT VALUES;

INSERT INTO users(user_name) VALUES ('adam');
INSERT INTO users(user_name) VALUES ('michal');
INSERT INTO users(user_name) VALUES ('jan');
INSERT INTO users(user_name) VALUES ('alex');